#include <QSqlDatabase>
#include <QFile>
#include <QSqlQuery>
#include <QSqlError>

#include <QDebug>


#include "sqlitecommands.h"

SQLiteCommands::SQLiteCommands(QString databaseLocation)
{
    setDbLocation(databaseLocation);
}

bool SQLiteCommands::CreateTables()
{
    //Check if the database exist
    if (!QFile(getDbLocation()).exists())
    {
        qDebug() << "Database not found!";
        return false;
    }

    //check if the resource is defined.
    //in this we have all the commands to create tables into db
    QString cmds = GetCommands();
    if (cmds.isEmpty())
    {
        qDebug() << "Commands not defined!";
        return false;
    }

    //open the database and create tables
    qDebug() << "Trying to open db: " << getDbLocation();
    QSqlDatabase db;
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(getDbLocation());

    if (db.open())
    {
        qDebug() << "DB opened!";
        QSqlQuery query(cmds, db);
        query.exec();

        if (query.numRowsAffected() == -1)
        {
            db.close();
            qDebug() << "Error in creating tables!";
            qDebug() << "Error: " << query.lastError().text();
            return false;
        }
        db.close();
        return true;
    }

    db.close();
    qDebug() << "Database cannot be opened!";
    return false;
}

QString SQLiteCommands::GetCommands()
{
    QFile f(":/sqlite_commands/CreateTables.txt");
    if (f.open(QIODevice::ReadOnly))
    {
        QString s = f.readAll();
        qDebug().noquote() << s;

        return s;
    }
    else
    {
        qDebug() << "Resource cannot be opened!";
    }
    return "";
}

QString SQLiteCommands::getDbLocation() const
{
    return dbLocation;
}

void SQLiteCommands::setDbLocation(const QString &value)
{
    dbLocation = value;
}
