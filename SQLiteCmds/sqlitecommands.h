#ifndef SQLITECOMMANDS_H
#define SQLITECOMMANDS_H

#include <QString>

class SQLiteCommands
{
public:
    SQLiteCommands(QString);

    bool CreateTables();

    QString getDbLocation() const;
    void setDbLocation(const QString &value);

private:
    QString GetCommands();

    QString dbLocation;
};

#endif // SQLITECOMMANDS_H
