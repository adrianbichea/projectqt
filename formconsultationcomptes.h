#ifndef FORMCONSULTATIONCOMPTES_H
#define FORMCONSULTATIONCOMPTES_H

#include <QDialog>

#include "CPP/cPerson.h"

namespace Ui {
class FormConsultationComptes;
}

class FormConsultationComptes : public QDialog
{
    Q_OBJECT

public:
    explicit FormConsultationComptes(int, cPerson*, QWidget *parent = nullptr);
    ~FormConsultationComptes();

private:
    Ui::FormConsultationComptes *ui;

    void ShowComptes(int);
    void UpdateLabels(cPerson*);

    QString str(string);
    QString getAddressComplete(string, string);

    double GetTotalAccountsValue(int);
};

#endif // FORMCONSULTATIONCOMPTES_H
