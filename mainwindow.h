#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlDatabase>
#include <map>

#include "CPP/cBigData.h"
#include "Utils/appconfiguration.h"
#include "CPP/testcpp.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionPrivates_triggered();

    void on_actionDefine_database_triggered();

    void on_actionProfessionals_triggered();

    void on_actionConsulation_des_comptes_triggered();

    void on_btnConsult_clicked();

    void closeEvent(QCloseEvent *event) override;

    void on_actionApply_bank_operations_triggered();

private:
    Ui::MainWindow *ui;
    testcpp testCPP;
    map<int, cPerson*> persons;
    QSqlDatabase database;

    const int PRIVATE = 1;
    const int PROFESSIONAL = 2;

    AppConfiguration appConfig;
    void RestartApplication();
    bool LoadTableHeaders(QString);
    void AddRows(int);
    int AddPersonToTable(cPerson*);
    void CheckDatabaseConnection();
};
#endif // MAINWINDOW_H
