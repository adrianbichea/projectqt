#include <QSqlDatabase>
#include <QSqlTableModel>
#include <QSqlQueryModel>
#include <QSqlQuery>

#include <QDebug>

#include "CPP/cProfessional.h"
#include "CPP/cPrivate.h"
#include "formconsultationcomptes.h"
#include "ui_formconsultationcomptes.h"
#include "Utils/sqlitedb.h"

FormConsultationComptes::FormConsultationComptes(int id, cPerson* person, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FormConsultationComptes)
{
    ui->setupUi(this);

    setWindowTitle("Consultation Compte");

    UpdateLabels(person);
    double accountValue = GetTotalAccountsValue(id);
    ui->lblTotalSolde->setText("Solde total: " + QString::number(accountValue, 'f', 2));
    ShowComptes(id);
}

FormConsultationComptes::~FormConsultationComptes()
{
    delete ui;
}

void FormConsultationComptes::ShowComptes(int id)
{

    if (SQLiteDB::Get().open())
    {
        QSqlQueryModel *model = new QSqlQueryModel();
        QString sPrep = "SELECT numcompte, datecreation, printf(\"%.2f\", solde) AS solde, "
                        "printf(\"%.2f\", decouvert) AS decouvert, numcli FROM comptes WHERE numcli=:id";
        QSqlQuery query(sPrep);
        query.prepare(sPrep);
        query.bindValue(":id", id);
        query.exec();
        model->setQuery(query);

        model->setHeaderData(0, Qt::Horizontal,  "Numéro de Compte");
        model->setHeaderData(1, Qt::Horizontal,  "Date Ouverture du compte");
        model->setHeaderData(2, Qt::Horizontal,  "Solde");
        model->setHeaderData(3, Qt::Horizontal,  "Montant du découvert autorisé");

        ui->tableView->setModel(model);
        ui->tableView->resizeColumnsToContents();

        ui->tableView->hideColumn(4);
    }
    else
    {
        qDebug() << "Database not opened!";
    }
}

void FormConsultationComptes::UpdateLabels(cPerson *p)
{
    ui->lblNom->setText(QString::fromStdString(p->GetName()));//if this is personal must add first name

    ui->lblLibelle->setText(getAddressComplete(p->GetAddress().GetStreetName(), p->GetAddress().GetStreetPlus()));

    ui->lblCPVille->setText(str(p->GetAddress().GetCP()) + ", " + str(p->GetAddress().GetCity()));
    ui->lblEmail->setText(str(p->GetEmail()));
    ui->lblInfo3->setVisible(false);
    ui->lblInfo4->setVisible(false);

    auto pers = dynamic_cast<cPrivate*>(p);
    if (pers)
    {
        ui->lblNom->setText(ui->lblNom->text() + " " + str(pers->GetSurname()));
        ui->lblInfo1->setText("Sexe: " + str(pers->GetSex()));
        ui->lblInfo2->setText("Date de naissance: " + str(pers->GetBirthday().ToString()));
        return;
    }
    auto pro = dynamic_cast<cProfessional*>(p);
    if (pro)
    {
        ui->lblInfo1->setText("Siret: " + str(pro->GetSiret()));
        ui->lblInfo2->setText("Status Juridique: " + str(pro->GetStatusToString()));
        ui->lblInfo3->setVisible(true);
        ui->lblInfo4->setVisible(true);
        ui->lblInfo3->setText(getAddressComplete(pro->GetProAddress().GetStreetName(), pro->GetProAddress().GetStreetPlus()));
        ui->lblInfo4->setText(str(pro->GetAddress().GetCP()) + ", " + str(pro->GetAddress().GetCity()));
    }
}

QString FormConsultationComptes::str(string s)
{
    return QString::fromStdString(s);
}

QString FormConsultationComptes::getAddressComplete(string s1, string s2)
{
    QString sRet = "";
    sRet = str(s1);
    QString aPlus = str(s2);
    if (!aPlus.isEmpty())
    {
        sRet += ", " + aPlus;
    }
    return sRet;
}

double FormConsultationComptes::GetTotalAccountsValue(int id)
{
    double value = 0.0;


    if (SQLiteDB::Get().open())
    {
        QString sPrep = "select SUM(solde) as somme from comptes WHERE numcli=:id";
        QSqlQuery query(sPrep);
        query.prepare(sPrep);
        query.bindValue(":id", id);
        query.exec();
        while(query.next())
        {
            value = query.value(0).toDouble();
        }
    }
    return value;
}

