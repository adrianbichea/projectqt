#ifndef TABLEITEMCREATOR_H
#define TABLEITEMCREATOR_H

#include <QTableWidgetItem>

class TableItemCreator
{
public:
    TableItemCreator();

    static QTableWidgetItem *Create(QString);
};

#endif // TABLEITEMCREATOR_H
