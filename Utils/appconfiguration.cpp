#include <QDebug>

#include "appconfiguration.h"

AppConfiguration::AppConfiguration()
{
    QSettings config(CONFIGURATION_FILENAME, QSettings::IniFormat);

    setDatabaseFilename(config.value(DATABASE_FILENAME).toString());
}

QString AppConfiguration::getDatabaseFilename() const
{
    return databaseFilename;
}

void AppConfiguration::setDatabaseFilename(const QString &value)
{
    databaseFilename = value;
}

void AppConfiguration::SaveDatabase()
{
    QSettings config(CONFIGURATION_FILENAME, QSettings::IniFormat);
    config.setValue(DATABASE_FILENAME, databaseFilename);
    qDebug() << "Configurations saved!";
}
