#ifndef SQLITEDB_H
#define SQLITEDB_H

#include <QSqlDatabase>

class SQLiteDB
{
public:
    SQLiteDB();

    static QSqlDatabase Get() { return db;};
    static void Set(const QSqlDatabase &database) { db = database;}

    static bool CheckDatabase();

private:
    static QSqlDatabase db;
};

#endif // SQLITEDB_H
