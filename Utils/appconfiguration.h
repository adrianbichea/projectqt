#ifndef APPCONFIGURATION_H
#define APPCONFIGURATION_H

#include <QString>
#include <QSettings>


class AppConfiguration
{
public:
    AppConfiguration();

    QString getDatabaseFilename() const;
    void setDatabaseFilename(const QString &value);

    void SaveDatabase();

private:
    const QString DATABASE_FILENAME = "Database/Location";
    const QString CONFIGURATION_FILENAME = "projectQT.ini";

    QString databaseFilename;

};

#endif // APPCONFIGURATION_H
