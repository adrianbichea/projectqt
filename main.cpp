#include "mainwindow.h"

#include <QApplication>
#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QFile fileCSS(":/stylesheet/myStyles");

    bool openOK = fileCSS.open(QIODevice::ReadOnly);

    if (openOK)
    {
        QString infosCSS = fileCSS.readAll();
        a.setStyleSheet(infosCSS);
        fileCSS.close();
        qDebug() << "Style loaded!";
    }
    else
    {
        qDebug() << "Error loading styles!";
    }

    MainWindow w;
    w.show();
    return a.exec();
}
