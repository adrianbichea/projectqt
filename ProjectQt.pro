QT       += core gui
QT       += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    CPP/Mouvements.cpp \
    CPP/OperationBank.cpp \
    CPP/cAddress.cpp \
    CPP/cBigData.cpp \
    CPP/cDate.cpp \
    CPP/cEmailChecker.cpp \
    CPP/cMouvementOperations.cpp \
    CPP/cMyExceptions.cpp \
    CPP/cOperationCode.cpp \
    CPP/cPerson.cpp \
    CPP/cPrivate.cpp \
    CPP/cProfessional.cpp \
    CPP/cUtils.cpp \
    CPP/testcpp.cpp \
    SQLiteCmds/sqlitecommands.cpp \
    Utils/appconfiguration.cpp \
    Utils/sqlitedb.cpp \
    Utils/tableitemcreator.cpp \
    formconsultationcomptes.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    CPP/Mouvements.h \
    CPP/OperationBank.h \
    CPP/cAddress.h \
    CPP/cBigData.h \
    CPP/cDate.h \
    CPP/cDefinitions.h \
    CPP/cEmailChecker.h \
    CPP/cMouvementOperations.h \
    CPP/cMyExceptions.h \
    CPP/cOperationCode.h \
    CPP/cPerson.h \
    CPP/cPrivate.h \
    CPP/cProfessional.h \
    CPP/cUtils.h \
    CPP/testcpp.h \
    SQLiteCmds/sqlitecommands.h \
    Utils/appconfiguration.h \
    Utils/sqlitedb.h \
    Utils/tableitemcreator.h \
    formconsultationcomptes.h \
    mainwindow.h

FORMS += \
    formconsultationcomptes.ui \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    sqlite_commands.qrc
