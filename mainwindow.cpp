#include <QApplication>
#include <QProcess>
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>

#include <typeinfo>

//SQLite
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlTableModel>

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "SQLiteCmds/sqlitecommands.h"
#include "Utils/tableitemcreator.h"
#include "CPP/cPrivate.h"
#include "formconsultationcomptes.h"
#include "Utils/sqlitedb.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    CheckDatabaseConnection();

    ui->grpProPers->setVisible(false);
    ui->grpComptes->setVisible(false);
    persons = testCPP.getBigData()->GetPerson();
}

MainWindow::~MainWindow()
{
    //delete persons;
    delete ui;
}


void MainWindow::on_actionPrivates_triggered()
{
    if (!LoadTableHeaders(":/PersonalTableHeader/PersonalTableHeader.txt"))
    {
        return;
    }

    ui->grpProPers->setVisible(true);
    ui->grpComptes->setVisible(false);
    ui->grpProPers->setTitle("Table Personal");
    AddRows(this->PRIVATE);
}

void MainWindow::on_actionProfessionals_triggered()
{
    if (!LoadTableHeaders(":/ProfessionalTableHeader/ProfessionalTableHeader.txt"))
    {
        return;
    }

    ui->grpProPers->setVisible(true);
    ui->grpComptes->setVisible(false);
    ui->grpProPers->setTitle("Table Professional");
    AddRows(this->PROFESSIONAL);
}

void MainWindow::on_actionDefine_database_triggered()
{
    if (!appConfig.getDatabaseFilename().isEmpty())
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Define new database...");
        msgBox.setText("Your database is already defined.\nDo you want to redefine it?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.setIcon(QMessageBox::Icon::Question);
        msgBox.setDefaultButton(QMessageBox::Cancel);
        if (msgBox.exec() == QMessageBox::No)
        {
            return;
        }
    }

    QString fileName = QFileDialog::getOpenFileName(this, QString("Open database"), QDir::currentPath(),
                                        "SQLite Files (*.db)");
    if (!fileName.isNull() || !fileName.isEmpty())
    {
        appConfig.setDatabaseFilename(fileName);
        appConfig.SaveDatabase();
        RestartApplication();
    }
    if (!appConfig.getDatabaseFilename().isEmpty())
    {
        ui->lblWarning->setVisible(false);
    }
}

void MainWindow::RestartApplication()
{
    qApp->quit();
    QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
}

bool MainWindow::LoadTableHeaders(QString file)
{
    QFile f(file);
    if (!f.open(QIODevice::ReadOnly))
    {
        return false;
    }

    ui->tableWidget->clear();
    ui->tableWidget->setRowCount(0);

    QString allText = f.readAll();

    auto lines = allText.split("\r\n");

    ui->tableWidget->setColumnCount(lines.count());
    for (int i(0); i < lines.count(); i++)
    {
        ui->tableWidget->setHorizontalHeaderItem(i, TableItemCreator::Create(lines[i]));
    }


    ui->tableWidget->resizeRowsToContents();
    return true;
}

void MainWindow::AddRows(int mode)
{
    for (auto it = persons.begin(); it != persons.end(); it++)
    {
        auto p = it->second;

        if (mode == PRIVATE)
        {
            auto priv = dynamic_cast<cPrivate*>(p);
            if (priv)
            {
                int idx = AddPersonToTable(p);
                ui->tableWidget->setItem(idx, 7, new QTableWidgetItem(QString::fromStdString(priv->GetSurname())));
                ui->tableWidget->setItem(idx, 8, new QTableWidgetItem(QString::fromStdString(priv->GetSex())));
                ui->tableWidget->setItem(idx, 9, new QTableWidgetItem(QString::fromStdString(priv->GetBirthday().ToString())));
            }
        }
        else if (mode == PROFESSIONAL)
        {
            auto pro = dynamic_cast<cProfessional*>(p);
            if (pro)
            {
                int idx = AddPersonToTable(p);
                ui->tableWidget->setItem(idx, 7, new QTableWidgetItem(QString::fromStdString(pro->GetSiret())));
                ui->tableWidget->setItem(idx, 8, new QTableWidgetItem(QString::fromStdString(pro->GetStatusToString())));
                ui->tableWidget->setItem(idx, 9, new QTableWidgetItem(QString::fromStdString(pro->GetProAddress().GetStreetName())));
                ui->tableWidget->setItem(idx, 10, new QTableWidgetItem(QString::fromStdString(pro->GetProAddress().GetStreetPlus())));
                ui->tableWidget->setItem(idx, 11, new QTableWidgetItem(QString::fromStdString(pro->GetProAddress().GetCP())));
                ui->tableWidget->setItem(idx, 12, new QTableWidgetItem(QString::fromStdString(pro->GetProAddress().GetCity())));
            }
        }
    }
    ui->tableWidget->resizeColumnsToContents();
}

int MainWindow::AddPersonToTable(cPerson *p)
{
    ui->tableWidget->insertRow(ui->tableWidget->rowCount());
    auto idx = ui->tableWidget->rowCount() - 1;
    ui->tableWidget->setItem(idx, 0, new QTableWidgetItem(QString::number(p->GetId())));
    ui->tableWidget->setItem(idx, 1, new QTableWidgetItem(QString::fromStdString(p->GetName())));
    ui->tableWidget->setItem(idx, 2, new QTableWidgetItem(QString::fromStdString(p->GetAddress().GetStreetName())));
    ui->tableWidget->setItem(idx, 3, new QTableWidgetItem(QString::fromStdString(p->GetAddress().GetStreetPlus())));
    ui->tableWidget->setItem(idx, 4, new QTableWidgetItem(QString::fromStdString(p->GetAddress().GetCity())));
    ui->tableWidget->setItem(idx, 5, new QTableWidgetItem(QString::fromStdString(p->GetAddress().GetCP())));
    ui->tableWidget->setItem(idx, 6, new QTableWidgetItem(QString::fromStdString(p->GetEmail())));
    return idx;
}

void MainWindow::CheckDatabaseConnection()
{
    ui->lblWarning->setVisible(true);
    ui->lblWarning->setText("Please check \"File->Define database\" to setup your SQLite database");
    if (!appConfig.getDatabaseFilename().isEmpty())
    {
        ui->lblWarning->setVisible(false);

        if (SQLiteDB::Get().isOpen())
        {
            SQLiteDB::Get().close();
            SQLiteDB::Get().removeDatabase(appConfig.getDatabaseFilename());
        }

        database = QSqlDatabase::addDatabase("QSQLITE");
        database.setDatabaseName(appConfig.getDatabaseFilename());
        SQLiteDB::Set(database);
        SQLiteDB::Get().open();
        if (!SQLiteDB::CheckDatabase())
        {
            ui->lblWarning->setVisible(true);
            ui->lblWarning->setText("Database connection error!");
        }
        else
        {
            qDebug() << "Database opened!";
        }
    }
}

void MainWindow::on_actionConsulation_des_comptes_triggered()
{
    QSqlTableModel *model = new QSqlTableModel();
    model->setTable("comptes");
    model->select();

    ui->tableView->setModel(model);
    ui->grpProPers->setVisible(false);
    ui->grpComptes->setVisible(true);
    ui->tableView->resizeColumnsToContents();
}

void MainWindow::on_btnConsult_clicked()
{
    auto sm = ui->tableWidget->selectionModel();
    if (sm && sm->hasSelection())
    {
        int id = sm->selectedRows().at(0).data().toInt();
        FormConsultationComptes fcc(id, persons[id]);
        fcc.exec();
    }
    else
    {
        QMessageBox::warning(this, tr("My Application"), tr("Please select a row!"), QMessageBox::Ok, QMessageBox::Ok);
    }

}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("Exit...");
    msgBox.setText("Do you want to exit?");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msgBox.setIcon(QMessageBox::Icon::Question);
    msgBox.setDefaultButton(QMessageBox::Cancel);

    int reponse = msgBox.exec();

    switch(reponse)
    {
        case QMessageBox::Yes :
                event->accept(); //Valider l'event de fermeture
            break;
        default:
                event->ignore(); //Annuler l'event de fermeture
            break;
    }
}

void MainWindow::on_actionApply_bank_operations_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, QString("Open bank transfers"), QDir::currentPath(),
                                        "Text Files (*.txt)");
    if (!fileName.isNull() || !fileName.isEmpty())
    {

    }
}
