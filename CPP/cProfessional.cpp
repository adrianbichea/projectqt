#include <sstream>
#include "cProfessional.h"
#include "cUtils.h"

cProfessional::cProfessional(int id, string name, cAddress address, string email, string siret, eProfessionalStatus status, cAddress proAddress)
              :cPerson(id, name, address, email)
{
    SetSiret(siret);
    SetStatus(status);
    SetProAddress(proAddress);
}

cProfessional::~cProfessional()
{

}

void cProfessional::SetSiret(string value)
{
    if (value.length() != SIRET_SIZE)
    {
        std::stringstream ss;
        ss << "must be: " << SIRET_SIZE << " but is: " << value.length() << " on " << value;
        throw cMyExceptions(eMyException::EX_BAD_SIRET, ss.str());
    }
    m_siret = value;
}

string cProfessional::GetStatusToString()
{
    switch(m_status)//to change this if i have enough time to a static class
    {
        case eProfessionalStatus::EURL:
            return "EURL";
        case eProfessionalStatus::SA:
            return "SA";
        case eProfessionalStatus::SARL:
            return "SARL";
        case eProfessionalStatus::SAS:
            return "SAS";
        default:
            return "INVALID";
    }
}

string cProfessional::Show()
{
    auto s = cPerson::Info("Professional: ");
    s = cUtils::ReplaceString(s, "{$surname}", "");
    stringstream ss;
    ss << s << endl << ConcatenatePerson() << endl;
    return ss.str();
}

string cProfessional::ConcatenatePerson()
{
    stringstream ss;
    ss << "\tSiret: " << GetSiret() << endl;
    ss << "\t" << GetStatusToString() << endl;
    ss << "\t" << GetAddress().ConcatenateAddress() << endl;
    return ss.str();
}

