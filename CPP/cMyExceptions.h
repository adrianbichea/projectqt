#ifndef CMYEXCEPTIONS_H_INCLUDED
#define CMYEXCEPTIONS_H_INCLUDED

#include <exception>
#include <iostream>

using namespace std;

enum eMyException
{
    EX_UNKNOWN = 0,
    EX_INVALID_EMAIL,
    EX_MAX_STRING_LENGTH_REACHED,
    EX_ID_ALREADY_EXIST,
    EX_BAD_SIRET
};

class cMyExceptions : public exception
{
    public:
        cMyExceptions(int,int=0) throw();
        cMyExceptions(int,string="") throw();
        virtual ~cMyExceptions() throw();

        virtual const char* what() const throw();

    private:
        void GetMessage(int, string) const;
        mutable string message;
        int numError;
};

#endif // CMYEXCEPTIONS_H_INCLUDED
