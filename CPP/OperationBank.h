#ifndef OPERATIONBANK_H_INCLUDED
#define OPERATIONBANK_H_INCLUDED

#include "cOperationCode.h"

class OperationBank
{
    public:
        OperationBank(int, string, int, double);
        ~OperationBank(){}

        int GetOperationNumber() { return m_operationNumber; }
        void SetOperationNumber(int value) { m_operationNumber = value; }

        string GetOperationDate() { return m_operationDate; }
        void SetOperationDate(string value) { m_operationDate = value; }

        int GetOperationCode() { return m_operationCode; }
        void SetOperationCode(int value) { m_operationCode = value; }

        double GetOperationValue() { return m_operationValue; }
        void SetOperationValue(double value) { m_operationValue = value; }

    private:
        int m_operationNumber;
        string m_operationDate;
        int m_operationCode;
        double m_operationValue;
};

#endif // OPERATIONBANK_H_INCLUDED
