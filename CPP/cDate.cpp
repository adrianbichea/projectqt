#include "cDate.h"
#include "cUtils.h"

cDate::cDate()
{
    SetNowDate();
}

cDate::cDate(int day, int month, int year)
{
    if (IsValidData())
    {
        SetDate(day, month, year);
    }
    else
    {
        SetNowDate();
    }
}

cDate::~cDate()
{

}

void cDate::SetDate(int day, int month, int year)
{
    m_day = day;
    m_month = month;
    m_year = year;
}

string cDate::ToString()
{
    stringstream ss;
    ss << cUtils::IntToString(m_day, 2) << "-" << cUtils::IntToString(m_month, 2) << "-";
    ss.precision(4);
    ss << m_year;
    return ss.str();
}

void cDate::SetNowDate()
{
    time_t t = time(NULL);
    tm* now = localtime(&t);
    m_year = now->tm_year + 1900;
    m_month = now->tm_mon + 1;
    m_day = now->tm_mday;
}
