#ifndef CDATE_H_INCLUDED
#define CDATE_H_INCLUDED

#include <sstream>
#include <iostream>
#include <ctime>

using namespace std;

class cDate
{
    public:
        cDate();
        cDate(int day, int month, int year);
        ~cDate();

        void SetDate(int, int, int);

        int GetDay() { return m_day; }
        int GetMonth() { return m_month; }
        int GetYear() { return m_year;}

        string ToString();

    private:
        int m_day;
        int m_month;
        int m_year;

        bool IsValidData() { return true; }// ToDo
        void SetNowDate();
};

#endif // CDATE_H_INCLUDED
