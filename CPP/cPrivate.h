#ifndef CPRIVATE_H_INCLUDED
#define CPRIVATE_H_INCLUDED

#include "cPerson.h"
#include "cDate.h"
#include "cUtils.h"

enum eSex
{
    MALE = 0,
    FEMALE
};

class cPrivate : public cPerson
{
    public:
        cPrivate(int, string, cAddress, string, string, int, int, int, eSex);
        ~cPrivate();

        string GetSurname();
        void SetSurname(string value);

        cDate GetBirthday() { return m_birthday; }
        void SetBirthday(int day, int month, int year) { m_birthday.SetDate(day, month, year); }

        string GetSex();
        void SetSex(eSex value) { m_sex = value; }

        string Show() override;
    private:
        string m_surname;
        cDate m_birthday;
        eSex m_sex;

        string ConcatenatePerson(string);
};

#endif // CPRIVATE_H_INCLUDED
