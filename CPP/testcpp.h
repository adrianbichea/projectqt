#ifndef TESTCPP_H
#define TESTCPP_H

#include "cPerson.h"
#include "cBigData.h"
#include "cAddress.h"
#include "cProfessional.h"
#include "cUtils.h"

#include "cOperationCode.h"
#include "Mouvements.h"

class testcpp
{
public:
    testcpp(bool = false);
    ~testcpp();

    cBigData *getBigData() const;

private:
    void execute(bool);
    void Initialize();

    cBigData *bigData;
};

#endif // TESTCPP_H
