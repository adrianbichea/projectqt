#include "OperationBank.h"

OperationBank::OperationBank(int n, string date, int code, double value)
{
    SetOperationNumber(n);
    SetOperationDate(date);
    SetOperationCode(code);
    SetOperationValue(value);
}
