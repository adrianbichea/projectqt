#include "cMyExceptions.h"
#include "cBigData.h"

cBigData::~cBigData()
{
    for (auto it = begin(m_persons); it != end(m_persons); it++)
    {
        if (it->second != nullptr)
        {
            delete(it->second);
        }
    }
    m_persons.clear();
}

void cBigData::AddNew(cPerson* person)
{
    if (m_persons.find(person->GetId()) == m_persons.end())
    {
        m_persons[person->GetId()] = person;
        return;
    }
    throw cMyExceptions(eMyException::EX_ID_ALREADY_EXIST, person->GetId());
}

string cBigData::Show()
{
    stringstream ss;
    ss << "--------- SHOW ALL ----------" << endl;
    for (auto it = begin(m_persons); it != end(m_persons); it++)
    {
        ss << it->second->Show();
    }
    return ss.str();
}

void cBigData::WriteToDatabase()
{
    //ToDo write to database
    for (auto it = begin(m_persons); it != end(m_persons); it++)
    {

    }
}

map<int, cPerson *> cBigData::GetPerson() const
{
    return m_persons;
}






