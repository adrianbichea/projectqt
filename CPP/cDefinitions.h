#ifndef CDEFINITIONS_H_INCLUDED
#define CDEFINITIONS_H_INCLUDED

#define MAX_STRING_LENGTH 50

//operations code
#define OP_CODE_DAB "DAB"
#define OP_CODE_DEPOT "DEPOT"
#define OP_CODE_CB "CB"

#define ANOMALIES_FILE "anomalies.txt"

#endif // CDEFINITIONS_H_INCLUDED
