#ifndef CEMAILCHECKER_H_INCLUDED
#define CEMAILCHECKER_H_INCLUDED

#include <iostream>
using namespace std;

class cEmailChecker
{
    public:
        static bool IsEmailValid(string);

    private:
        static bool IsChar(char);
};

#endif // CEMAILCHECKER_H_INCLUDED
