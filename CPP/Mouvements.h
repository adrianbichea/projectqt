#ifndef MOUVEMENTS_H_INCLUDED
#define MOUVEMENTS_H_INCLUDED

#include <vector>

#include "OperationBank.h"


class Mouvements
{
    public:
        Mouvements();
        ~Mouvements();

        string ShowAccounts();

    private:
        void LogAnomalies(int, int, long, double);
};

#endif // MOUVEMENTS_H_INCLUDED
