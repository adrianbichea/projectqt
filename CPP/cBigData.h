#ifndef CBIGDATA_H_INCLUDED
#define CBIGDATA_H_INCLUDED

#include <map>
#include <memory>
#include <QList>

#include "cPrivate.h"

class cBigData
{
    public:
        cBigData() {}
        ~cBigData();

        void AddNew(cPerson*);

        string Show();
        void WriteToDatabase();


        map<int, cPerson *> GetPerson() const;

private:
        map<int, cPerson*> m_persons;
};

#endif // CBIDDATA_H_INCLUDED
